package ru.t1.volkova.tm.service;

import ru.t1.volkova.tm.api.service.ITaskService;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.api.repository.ITaskDTORepository;
import ru.t1.volkova.tm.dto.TaskDTO;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private ITaskDTORepository taskRepository;

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return taskRepository.findAll();
    }

    @Transactional
    @Override
    public void saveAll(@NotNull List<TaskDTO> projects) {
        taskRepository.saveAll(projects);
    }

    @Transactional
    @Override
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable String id) {
        if (id == null) return null;
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    @Transactional
    @Override
    public TaskDTO create(@Nullable TaskDTO task) {
        if (task == null) {
            return null;
        }
        return taskRepository.save(task);
    }

    @Nullable
    @Transactional
    @Override
    public TaskDTO updateById(@Nullable String id) {
        if (id == null) return null;
        @Nullable final TaskDTO task = findById(id);
        if (task == null) {
            return null;
        }
        return taskRepository.save(task);
    }

    @Nullable
    @Transactional
    @Override
    public TaskDTO update(@Nullable TaskDTO task) {
        if (task == null) {
            return null;
        }
        return taskRepository.save(task);
    }

    @Transactional
    @Override
    public void deleteById(@Nullable String id) {
        if (id == null) return;
        taskRepository.deleteById(id);
    }

}

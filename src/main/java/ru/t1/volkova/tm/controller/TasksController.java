package ru.t1.volkova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.volkova.tm.api.repository.IProjectDTORepository;
import ru.t1.volkova.tm.api.repository.ITaskDTORepository;

@Controller
public class TasksController {

    @Autowired
    private ITaskDTORepository taskRepository;

    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", taskRepository.findAll());
    }

}

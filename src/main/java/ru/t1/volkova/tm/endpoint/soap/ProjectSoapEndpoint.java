package ru.t1.volkova.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.dto.*;

import java.util.List;

@Endpoint
public class ProjectSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://tm.volkova.t1.ru/dto";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse projectFindById(@RequestPayload final ProjectFindByIdRequest request) {
        String id = request.getId();
        return new ProjectFindByIdResponse(projectService.findById(id));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse projectCreate(@RequestPayload final ProjectCreateRequest request) {
        ProjectDTO project = request.getProject();
        projectService.create(project);
        return new ProjectCreateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = NAMESPACE)
    public ProjectUpdateResponse projectUpdate(@RequestPayload final ProjectUpdateRequest request) {
        ProjectDTO project = request.getProject();
        projectService.update(project);
        return new ProjectUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse projectDeleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        String id = request.getId();
        projectService.deleteById(id);
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse projectFindAll(@RequestPayload final ProjectFindAllRequest request) {
        ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProject(projectService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateAllRequest", namespace = NAMESPACE)
    public ProjectCreateAllResponse projectCreateAll(@RequestPayload final ProjectCreateAllRequest request) {
        @NotNull List<ProjectDTO> projects = request.getProject();
        projectService.saveAll(projects);
        return new ProjectCreateAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateAllRequest", namespace = NAMESPACE)
    public ProjectUpdateAllResponse projectUpdateAll(@RequestPayload final ProjectUpdateAllRequest request) {
        @NotNull List<ProjectDTO> projects = request.getProject();
        projectService.saveAll(projects);
        return new ProjectUpdateAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse projectDeleteAll(@RequestPayload final ProjectDeleteAllRequest request) {
        projectService.removeAll();
        return new ProjectDeleteAllResponse();
    }

}

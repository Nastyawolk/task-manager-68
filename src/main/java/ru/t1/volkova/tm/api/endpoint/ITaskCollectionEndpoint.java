package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.volkova.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskCollectionEndpoint {

    @GetMapping()
    @NotNull
    @WebMethod
    List<TaskDTO> get();

    @PostMapping
    @WebMethod
    void post(@RequestBody @NotNull List<TaskDTO> tasks);

    @PutMapping
    @WebMethod
    void put(@RequestBody @NotNull List<TaskDTO> tasks);

    @DeleteMapping()
    @WebMethod
    void delete();

}

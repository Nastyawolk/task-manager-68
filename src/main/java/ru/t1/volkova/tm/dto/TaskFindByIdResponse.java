package ru.t1.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "task"
})
@XmlRootElement(name = "taskFindByIdResponse")
@Setter
@Getter
@NoArgsConstructor
public class TaskFindByIdResponse {

    protected TaskDTO task;

    public TaskFindByIdResponse(TaskDTO task) {
        this.task = task;
    }

}

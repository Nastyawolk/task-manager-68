package ru.t1.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

import org.springframework.format.annotation.DateTimeFormat;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
public class ProjectDTO {

    @NotNull
    @Id
    @Column(nullable = false)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(nullable = false)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "date_start")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart = new Date();

    @NotNull
    @Column(name = "date_finish")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish = new Date();

    public ProjectDTO(@NotNull final String name) {
        this.name = name;
    }

}

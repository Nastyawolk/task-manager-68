package ru.t1.volkova.tm.dto;

import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "task"
})
@XmlRootElement(name = "taskCreateAllRequest")
@Setter
public class TaskCreateAllRequest {

    @XmlElement(required = true)
    protected List<TaskDTO> task;


    public List<TaskDTO> getTask() {
        if (task == null) {
            task = new ArrayList<>();
        }
        return this.task;
    }

}

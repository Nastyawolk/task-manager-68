package ru.t1.volkova.tm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "task"
})
@Getter
@Setter
@XmlRootElement(name = "taskFindAllResponse")
public class TaskFindAllResponse {

    protected List<TaskDTO> task;

    public List<TaskDTO> getTask() {
        if (task == null) {
            task = new ArrayList<TaskDTO>();
        }
        return this.task;
    }

}

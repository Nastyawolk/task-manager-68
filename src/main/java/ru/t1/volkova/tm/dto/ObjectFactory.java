package ru.t1.volkova.tm.dto;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectDTO createProjectDTO() {
        return new ProjectDTO();
    }

    public ProjectCreateRequest createProjectCreateRequest() {
        return new ProjectCreateRequest();
    }

    public ProjectCreateResponse createProjectCreateResponse() {
        return new ProjectCreateResponse();
    }

    public ProjectUpdateRequest createProjectUpdateRequest() {
        return new ProjectUpdateRequest();
    }

    public ProjectUpdateResponse createProjectUpdateResponse() {
        return new ProjectUpdateResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectCreateAllRequest createProjectCreateAllRequest() {
        return new ProjectCreateAllRequest();
    }

    public ProjectCreateAllResponse createProjectCreateAllResponse() {
        return new ProjectCreateAllResponse();
    }

    public ProjectUpdateAllRequest createProjectUpdateAllRequest() {
        return new ProjectUpdateAllRequest();
    }

    public ProjectUpdateAllResponse createProjectUpdateAllResponse() {
        return new ProjectUpdateAllResponse();
    }

    public ProjectDeleteAllRequest createProjectDeleteAllRequest() {
        return new ProjectDeleteAllRequest();
    }

    public ProjectDeleteAllResponse createProjectDeleteAllResponse() {
        return new ProjectDeleteAllResponse();
    }

}

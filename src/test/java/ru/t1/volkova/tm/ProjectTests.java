/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package ru.t1.volkova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.t1.volkova.tm.client.ProjectRestEndpointClient;
import ru.t1.volkova.tm.dto.ProjectDTO;
import ru.t1.volkova.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aavolkova
 */
public class ProjectTests {

    private static final long SIZE = 5;
    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/project";
    @NotNull
    private static final RestTemplate template = new RestTemplate();
    @NotNull
    private static final HttpHeaders headers = new HttpHeaders();
    private static ProjectRestEndpointClient client;
    @Nullable
    private static List<ProjectDTO> projectList;

    @BeforeClass
    public static void initTest() {
        headers.setContentType(MediaType.APPLICATION_JSON);
        client = new ProjectRestEndpointClient(BASE_URL, template, headers);
        createProjects();
    }

    public static void createProjects() {
        projectList = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            @NotNull ProjectDTO project = new ProjectDTO();
            project.setName("Project" + i);
            project.setDescription("description" + i);
            client.save(project);
            projectList.add(project);
        }
    }

    @AfterClass
    public static void deleteProjects() {
        if (projectList == null) return;
        for (final ProjectDTO project : projectList) {
            client.deleteById(project.getId());
        }
    }

    // PROJECT TESTS

    //GET
    @Test
    @Category(IntegrationCategory.class)
    public void projectFindByIdTest() {
        final ProjectDTO project = client.findById(projectList.get(0).getId());
        assertEquals(projectList.get(0).getId(), project.getId());
    }

    //POST
    @Test
    @Category(IntegrationCategory.class)
    public void projectCreateTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("ProjectCreated");
        project.setDescription("descriptionCreated");
        client.save(project);
        projectList.add(project);
        assertEquals(client.findAll().size(), projectList.size());
    }

    //UPDATE
    @Test
    @Category(IntegrationCategory.class)
    public void projectUpdateTest() {
        final ProjectDTO project = projectList.get(0);
        project.setName("ProjectNew");
        project.setDescription("descriptionNew");
        client.update(project);
        ProjectDTO projectUpdated = client.findById(project.getId());
        assertEquals(projectList.size(), SIZE);
        assertEquals(project.getName(), projectUpdated.getName());
        assertEquals(project.getDescription(), projectUpdated.getDescription());
    }

    //DELETE
    @Test
    @Category(IntegrationCategory.class)
    public void projectDeleteByidTest() {
        client.deleteById(projectList.get(1).getId());
        final ProjectDTO project = client.findById(projectList.get(1).getId());
        assertNull(project);
    }

    // PROJECT COLLECTION TESTS

    //GET
    @Test
    @Category(IntegrationCategory.class)
    public void projectFindAllTest() {
        final List<ProjectDTO> projects = client.findAll();
        assertNotEquals(0, projects.size());
    }

    //POST
    @Test
    @Category(IntegrationCategory.class)
    public void projectCreateListTest() {
        final long newSize = 2;
        final List<ProjectDTO> newProjectList = new ArrayList<ProjectDTO>();
        for (int i = 0; i < newSize; i++) {
            @NotNull ProjectDTO project = new ProjectDTO();
            project.setName("Project_test" + i);
            project.setDescription("description_test" + i);
            newProjectList.add(project);
            projectList.add(project);
        }
        client.saveAll(newProjectList);
        final long size = client.findAll().size();
        assertEquals(size, SIZE + newSize);
    }

    //UPDATE
    @Test
    @Category(IntegrationCategory.class)
    public void projectUpdateAllTest() {
        final String NewName = "UpdName";
        final String NewDesc = "UpdDesc";
        for (final ProjectDTO project : projectList) {
            project.setName(NewName);
            project.setDescription(NewDesc);
        }
        client.saveAll(projectList);
        final List<ProjectDTO> updProjectList = client.findAll();
        for (final ProjectDTO project : updProjectList) {
            assertEquals(project.getName(), NewName);
            assertEquals(project.getDescription(), NewDesc);
        }
    }

    //DELETE
    @Test
    @Category(IntegrationCategory.class)
    public void projectDeleteAllTest() {
        client.deleteAll();
        assertEquals(0, client.findAll().size());
        createProjects();
    }

}
